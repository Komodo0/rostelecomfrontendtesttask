/**
 * Класс глобальных настроек приложения
 */
function Properties() {

    this.properties = {

        "requestUrl": "http://81.177.101.143:30080/test.json",
        "elementsNumberToShow": 4

    };

    this.get = function (propertyName) {
        return this.properties[propertyName] || null;
    };

    this.set = function (propertyName, propertyValue) {
        this.properties.propertyName = propertyValue;
    };

}