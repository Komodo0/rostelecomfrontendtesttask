/**
 * Класс-контроллер элемента
 * @param elementsNumberToShow - по сколько элементов в строке показываем
 * @param elementService - сервис-клас для работы с элементоами
 */
function ElementController(elementsNumberToShow, elementService) {

    this.elementsNumberToShow = elementsNumberToShow;
    this.elementService = elementService;

    /**
     * Добавление строки элементов на страницу
     */
    this.addMoreElements = function () {
        this.elementService.addNewElements(elementsNumberToShow);
    };

    /**
     * Удаление элемента
     */
    this.removeElement = function () {

        element = this.parentNode;
        elementLineNode = element.parentNode;

        if (elementLineNode.childNodes.length === 1) {
            elementLineNode.parentNode.removeChild(elementLineNode);
        } else {
            elementLineNode.removeChild(element);

            elementLineNode.childNodes.forEach(function (node, i, nodes) {
                node.style.width = 99 / nodes.length + "%";
            });
        }

    };

}

/**
 * Контроллер кнопки Download
 */
function DownloadButtonController() {

    /**
     * Добавление строки жлементов
     */
    this.addElement = function () {
        beans.getBean("elementController").addMoreElements();
    };

    /**
     * Инициализация бина при создании
     */
    this.init = function () {
        document.getElementById("showMorePictures").onclick = this.addElement;
    }

}