/**
 * Объект, содержащий определения бинов.
 * @type {BeansDefenition}
 */
var beans = new BeansDefenition();

/**
 * Инициализация приложения
 */
window.onload = function () {
    try {

        //Получаем настройки приложения
        var properties = new Properties();

        beans.addBean("elementView", new ElementView());

        beans.addBean("elementService", new ElementService(
            properties.get("requestUrl"),
            beans.getBean("elementView")));

        beans.addBean("elementController", new ElementController(
            properties.get("elementsNumberToShow"),
            beans.getBean("elementService")));

        beans.addBean("downloadButtonController", new DownloadButtonController(
            beans.getBean("elementController")));

        //После того, как мы назначили зависимости, бины можно проинициализировать (выполнить init-метод во всех бинах)
        beans.initBeans();

    } catch (err) {
        alert("Ошибка инициализации приложения! Попробуйте перезагрузить страницу!");
        console.log('Ошибка ' + err.name + ":" + err.message + "\n" + err.stack);
    }
};

/**
 * Класс - содержащий массив бинов
 */
function BeansDefenition() {

    this.beans = {};

    this.getBean = function (beanName) {
        return this.beans[beanName] || null;
    };

    this.addBean = function (beanName, bean) {
        this.beans[beanName] = bean;
    };

    this.removeBean = function (beanName) {
        delete this.beans[beanName];
    };

    /**
     * Вызывает метод init() в созданных бинах.
     */
    this.initBeans = function () {
        for (var beanName in this.beans) {
            if (typeof this.getBean(beanName).init !== "undefined" && typeof this.getBean(beanName).init === "function") {
                this.getBean(beanName).init();
            }
        }
    }

}