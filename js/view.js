/**
 * Класс-представления элемента
 */
function ElementView() {

    /**
     * Создает один элемент
     * @param elementData - объект, содержащий соответствующие поля для заполнения жлемента
     * @param elementWidth - ширина создаваемого элемента
     * @returns {Element|*} - возвращает DOM-элемент
     */
    this.createElementView = function createElementViev(elementData, elementWidth) {

        elementView = document.createElement("div");
        elementView.className = "element";
        elementView.style.width = elementWidth;
        elementView.style.backgroundImage = "url('" + elementData.imageUrl + "')";

        elementTitle = document.createElement("div");
        elementTitle.className = "elementContent elementAge";
        elementTitle.innerText = "" + elementData.age || "UNDEFINED";

        elementText = document.createElement("div");
        elementText.className = "elementContent elementText";
        elementText.innerText = elementData.text.toUpperCase() || "UNDEFINED";

        elementDeleteButton = document.createElement("a");
        elementDeleteButton.className = "btn elementContent elementDeleteButton";
        elementDeleteButton.setAttribute("href", "#");
        elementDeleteButton.innerText = "DELETE";
        elementDeleteButton.onclick = beans.getBean("elementController").removeElement;

        elementView.appendChild(elementTitle);
        elementView.appendChild(elementText);
        elementView.appendChild(elementDeleteButton);

        return elementView;
    };

    /**
     * Генерирует строку элементов
     * @param elementsData - массив объектов, содержащих поля для заполнения элементов
     * @returns {Element|*} - возвращает DOM-элемент строку, наполненную элементами.
     */
    this.createElementsRowView = function (elementsData) {

        elementLineView = document.createElement("div");
        elementLineView.className = "elementsRow";

        for (i = 0; i < elementsData.length; i++){
            elementLineView.appendChild(this.createElementView(elementsData[i], 99/elementsData.length + "%"));
        }

        return elementLineView;
    }

}