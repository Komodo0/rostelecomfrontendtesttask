/**
 * Сервис-класс для работы с элементами
 * @param requestUrl - ссылка для скачивания JSON-файла с инфомрацией
 * @param elementView - инъекция класса-представления
 */
function ElementService(requestUrl, elementView) {

    this.requestUrl = requestUrl;
    this.elementView = elementView;
    this.elementsNumberToShow = 4;

    this.downloadedElements = [];
    this.lastShownElementIndex = 0;

    /**
     * Скачать данные по элементам
     * @param requestUrl - ссылка для скачивания JSON-файла с инфомрацией
     */
    this.downloadElements = function (requestUrl) {
        var request = new XMLHttpRequest();

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    //Все ок
                    beans.getBean("elementService").downloadedElements = JSON.parse(request.responseText);
                    beans.getBean("elementService").addNewElements();
                } else {
                    //Ошибка при выполнении запроса
                    alert("Ошибка при выполнении Ajax-запроса к серверу!");
                }
            }
        };
        request.open('Get', new Properties().get("requestUrl"));
        request.send();
    };

    /**
     * Добавить элементы в DOM
     * @param elementsNumberToShow - количество отображаемых в строке элементов
     */
    this.addNewElements = function (elementsNumberToShow) {

        //Можем переопределить кол-во показываемых элементов
        if (typeof  elementsNumberToShow !== "undefined") {
            this.elementsNumberToShow = elementsNumberToShow;
        }

        //Если мы еще не скачали элементы - идем качать и потом заново в этот метод
        if (this.downloadedElements.length === 0) {
            this.downloadElements(this.requestUrl);
            return;
        }

        //Выбираем из всех скачанных элементов те, которые будем показывать, если элементы кончились - по второму кругу не идем.
        data = [];
        for (i = this.lastShownElementIndex; i < this.lastShownElementIndex + this.elementsNumberToShow; i++) {
            if (typeof this.downloadedElements[i] !== undefined) {
                data.push(this.downloadedElements[i]);
            }
        }

        //Сдвигаем указатель на последний показанный элемент
        this.lastShownElementIndex += this.elementsNumberToShow;

        //Пихаем сгенерированные элементы в DOM
        document.getElementById("elementsContainer").appendChild(this.elementView.createElementsRowView(data));
    };


}